<div class="minh-75vh d-flex">
    <div class="row w-100 my-auto mx-auto">
        <div class="col-11 col-sm-8 col-md-6 col-lg-4 col-xxl-3 mx-auto">
            <div class="card bg-dark border-secondary opacity-9">
                <div class="card-header border-secondary">
                    <h6 class="text-secondary text-uppercase my-1">
                        Login
                    </h6>
                </div>
                <div class="card-body">
                    <form class="text-start px-3"
                        action=""
                        autocomplete="off"
                        method="post">
                        <div class="form-group mb-3">
                            <label class="text-secondary mb-2 ms-1">Login</label>
                            <input class="form-control rounded-pill bg-dark text-white border-secondary" 
                                name="username"
                                maxlength="45"
                                tabindex="1"
                                required
                                type="text" 
                                placeholder="User login"/>
                        </div>
                        <div class="form-group mb-3">
                            <label class="text-secondary mb-2 ms-1">Password</label>
                            <input class="form-control rounded-pill bg-dark text-white border-secondary" 
                                name="userpass"
                                maxlength="45"
                                tabindex="2"
                                required
                                type="password" 
                                placeholder="Enter password"/>
                        </div>
                        <div class="w-100 small border-bottom border-secondary text-secondary">
                            <p>
                                Not registered? Register
                                <a href="register.html" class="fw-bold text-decoration-none text-secondary">Now</a>
                            </p>
                        </div>
                        <div class="w-100 text-end py-3">
                            <input type="reset" 
                                class="btn btn-secondary rounded-pill me-1" 
                                value="Clear"                                
                                tabindex="6"/>
                            <input type="submit" 
                                class="btn btn-secondary rounded-pill" 
                                name="login"
                                value="Login"
                                tabindex="5"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>