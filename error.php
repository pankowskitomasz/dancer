<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!isset($_SESSION["errorMessage"])){
    $_SESSION["errorMessage"] = "Unfortunately your message was not send due to technical ";
    $_SESSION["errorMessage"] .= "problems. Please try again later or contact with us by phone.";
}

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,shrink-to-fit=no"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
        <link rel="icon" href="img/favicon.png"/>
        <title> | Login</title>
    </head>
    <body class="minh-100vh">
        <header class="position-absolute w-100">
            <nav class="navbar navbar-dark navbar-expand-md bg-transparent">
                <a href="#" class="navbar-brand ms-3">
                    <img src="img/navbar_logo.png" class="img-fluid" alt="logo"/>
                </a>
                <button class="navbar-toggler me-3 border-dark" data-bs-toggle="collapse" data-bs-target="#mainnav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mainnav">
                    <ul class="navbar-nav text-end ms-auto text-uppercase text-shadow">
                        <li class="nav-item pe-3">
                            <a href="index.html" class="nav-link text-white">Home</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="product.html" class="nav-link text-white">Product</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="promo.html" class="nav-link text-white">Promo</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="about.html" class="nav-link text-white">About</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="contact.html" class="nav-link text-white">Contact</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="user.php" class="nav-link text-white"><span class="fa fa-user"></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <main>   
            <section class="container error-s1 d-flex py-3 min-vh-100">
                <div class="my-auto text-center text-md-left px-md-5">               
                    <div class="col-10 col-sm-8 col-md-6 offset-1 offset-sm-2 offset-md-3 text-center">
                        <div class="alert alert-danger">
                            <h3 class="text-center font-header">Error!</h3>
                            <p class="initialism">
                                <?php
                                    echo $_SESSION["errorMessage"];
                                ?>
                            </p>                  
                            <a href="contact.html" 
                                class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <footer class="container-fluid d-flex text-dark align-items-center bg-dark text-white pt-3 opacity-9 border-top">
            <div class="row mx-0 w-100 small opacity-9">
                <div class="col-12 col-md-6 col-lg-5 text-center text-md-start">
                    <h6 class="text-uppercase mb-3">
                        Session and Enrollment
                    </h6>
                    <p class="initialism fw-normal">
                        Please be aware of the start and end dates of each session. Also keep an eye 
                        out for when new enrollment opens, which is typically in April for Fall/Winter, 
                        October for Spring/Summer. A $35 registration fee per student will be charged 
                        to hold your child’s spot for the future session.
                    </p>
                </div>
                <div class="col-12 col-md-6 col-lg-7 text-center text-md-end">                    
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-facebook text-white"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-instagram text-white"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-twitter text-white"></span>
                            </a>
                        </li>
                    </ul>       
                </div>
                <div class="col-12 text-center border-top">
                    <p class="mb-1">
                        Copyright &copy; 2021-2022 Tomasz Pankowski. 
                        <a href="privacy.html" class="fw-bold text-white text-decoration-none">
                            Privacy policy
                        </a>
                    </p>
                </div>
            </div>
        </footer>
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
<?php $_SESSION["errorMessage"]=null ?>